import wget
import os



sourceURL = "http://localhost:3333/"

def installPackage(packageName, installPath):
	wget.download(sourceURL + "?file=" + packageName, installPath + packageName)
	print(packageName + " installed!")


def updateRepos():

	if(os.path.exists(os.getenv("HOME") + "/.please/" + "packagelists.json")):
		os.remove("/home/gregory/.please/packagelists.json")
	
	wget.download(sourceURL + "?file=updatereps", os.getenv("HOME") + "/.please/" + "packagelists.json")

updateRepos()